let produits = document.querySelectorAll(".nb-produit");
let plusButtons = document.querySelectorAll(".plus");
let moinsButtons = document.querySelectorAll(".moins");
let ajouterPanierButtons = document.querySelectorAll(".ajouter-panier");
let viderPanierInteraction = document.getElementById("viderPanier");
let totalPanierElement = document.querySelector(".panier");
let prix = document.querySelectorAll(".prix");
let favButtons = document.querySelectorAll(".fa-heart");
let sousTotal = document.querySelectorAll(".sous-total");
let voirPlus = document.querySelector(".voir");
let plusArticle = document.querySelector(".affaires2");
let notificationBadge = document.querySelector(".notification-badge");
let notificationContainer = document.querySelector(".notification-container");
let notificationMessage = document.querySelector(".notification-message");

let maxProduits = 15;
let totalPanier = 0;

function showNotification(message) {
  notificationMessage.textContent = message;
  notificationContainer.querySelector(".notification").classList.add("show");
  setTimeout(() => {
    notificationContainer
      .querySelector(".notification")
      .classList.remove("show");
  }, 3000);
}

favButtons.forEach(button => {
  button.addEventListener("click", () => {
    if (button.style.color === "white" || button.style.color === "") {
      button.style.color = "#FF6347";
      button.classList.toggle("active");
    } else {
      button.style.removeProperty("color");
      button.classList.remove("active");
    }
  });
});

plusButtons.forEach(button => {
  button.addEventListener("click", () => {
    let produit = button.parentElement.querySelector(".nb-produit");
    let i = parseInt(produit.innerText);

    let totalProduits = 0;
    produits.forEach(p => {
      totalProduits += parseInt(p.innerText);
    });

    if (totalProduits < maxProduits) {
      i += 1;
      produit.innerText = `${i}`;
    } else {
      alert("Vos poches sont trop petites pour ajouter plus de 15 produits.");
    }

    calculerSousTotal();
  });
});

moinsButtons.forEach(button => {
  button.addEventListener("click", () => {
    let produit = button.parentElement.querySelector(".nb-produit");
    let i = parseInt(produit.innerText);
    if (i > 0) {
      i -= 1;
      produit.innerText = `${i}`;
    }

    calculerSousTotal();
  });
});

ajouterPanierButtons.forEach(button => {
  button.addEventListener("click", () => {
    let produitElement = button.parentElement;
    let nomProduit = produitElement.querySelector(".nom-produit").innerText;
    let quantiteElement = produitElement.querySelector(".nb-produit");
    let quantite = parseInt(quantiteElement.innerText);
    let prixTotal = parseFloat(
      produitElement.querySelector(".sous-total").innerText
    );
    let sousTotalElement = produitElement.querySelector(".sous-total");

    if (quantite > 0) {
      let message = `Ajouté au panier : ${nomProduit} (x${quantite}) - Total: $${prixTotal}`;
      showNotification(message);

      totalPanier += prixTotal;
      totalPanierElement.innerText = `$${totalPanier}`;

      quantiteElement.innerText = "0";
      sousTotalElement.innerText = "0";

      let totalArticles = parseInt(notificationBadge.innerText) + quantite;
      notificationBadge.innerText = totalArticles;
      viderPanierInteraction.classList.add("visible");
    } else {
      alert("Veuillez ajouter une quantité avant d'ajouter au panier.");
    }
  });
});

function calculerSousTotal() {
  produits.forEach((produit, index) => {
    let quantite = parseInt(produit.innerText);
    let prixElement = produit.parentElement.querySelector(".prix");
    let prix = parseFloat(prixElement.innerText);
    let sousTotalElement = produit.parentElement.querySelector(".sous-total");
    let total = quantite * prix;
    sousTotalElement.innerText = `${total}`;
  });
}

viderPanierInteraction.addEventListener("click", () => {
  if (confirm("Êtes-vous sûr de vouloir vider le panier ?")) {
    produits.forEach(produit => {
      produit.innerText = "0";
    });
    viderPanierInteraction.classList.remove("visible");
    totalPanier = 0;
    totalPanierElement.innerText = "$0";
    notificationBadge.innerText = "0";
  }
});

voirPlus.addEventListener("click", () => {
  if (voirPlus.innerText === "Voir plus de bonnes affaires") {
    plusArticle.style.display = "flex";
    voirPlus.innerText = "Voir moins d'articles";
  } else {
    plusArticle.style.display = "none";
    voirPlus.innerText = "Voir plus de bonnes affaires";
  }
});
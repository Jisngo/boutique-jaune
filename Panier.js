let produits = document.querySelectorAll('.nb-produit');
let plusButtons = document.querySelectorAll('.plus');
let moinsButtons = document.querySelectorAll('.moins');
let sousTotaux = document.querySelectorAll('.sous-total');
let prix = document.querySelectorAll('.prix');
let totalCommande = document.querySelector('.total-commande');
let viderPanierInteraction = document.getElementById("viderPanier");
let panierProduits = document.querySelector(".panier-produits");
let supprimerProduit = document.querySelectorAll('#poubelle');

let maxProduits = 15;

moinsButtons.forEach(button => {
    button.addEventListener('click', () => {
        let produit = button.parentElement.querySelector('.nb-produit');
        let i = parseInt(produit.innerText);
        if (i > 0) {
            i -= 1;
            produit.innerText = `${i}`;
        }
        if (i == 0) {
            if (confirm("Êtes-vous sûr de vouloir supprimer cet article ?")) {
                let article = button.parentElement.parentElement;
                let nbArticles = article.querySelector('.nb-produit');
                nbArticles.innerText = '0';
                article.style.display = 'none';
                calculerTotalPanier();
            }
        }
        calculerTotalPanier();
        calculSousTotal();
    });  
});

plusButtons.forEach(button => {
    button.addEventListener('click', () => {
        let produit = button.parentElement.querySelector('.nb-produit');
        let i = parseInt(produit.innerText);
        if (i < maxProduits) {
            i += 1;
            produit.innerText = `${i}`;
        }
        calculSousTotal();
        calculerTotalPanier();
    });  
});

function calculSousTotal() {
    let st = 0;
    produits.forEach(produit => {
      let quantite = parseInt(produit.innerText);
      let prixElement = produit.parentElement.querySelector('.prix');
      let prix = parseInt(prixElement.innerText);
      let sousTotalElement = produit.parentElement.querySelector('.sous-total');
      st = quantite * prix;
      sousTotalElement.innerText = `${st}`;
    });
}

function calculerTotalPanier() {
    let total = 0;
    produits.forEach(produit => {
      let quantite = parseInt(produit.innerText);
      let prixElement = produit.parentElement.querySelector('.prix');
      let prix = parseFloat(prixElement.innerText); 
      total += quantite * prix;
    });
    totalCommande.innerText = `Total de ma commande : $${total}`;
    if (total >= 75) {
        totalCommande.style.color = 'red';
    }
    else if (total >= 50) {
        totalCommande.style.color = 'orange';
    }
    else {
        totalCommande.style.color = 'green';
    }
  }

viderPanierInteraction.addEventListener("click", () => {
    if (confirm("Êtes-vous sûr de vouloir vider le panier ?")) {
        panierProduits.innerText = "Votre panier est vide 😢";
        totalCommande.innerText = `Total de ma commande : $0`;
        totalCommande.style.color = 'green';
    }
});

supprimerProduit.forEach(button => {
    button.addEventListener("click", () => {
        if (confirm("Êtes-vous sûr de vouloir supprimer cet article ?")) {
            let article = button.parentElement.parentElement;
            let nbArticles = article.querySelector('.nb-produit');
            nbArticles.innerText = '0';
            article.style.display = 'none';
            // if () {
            //     panierProduits.innerText = "Votre panier est vide 😢";
            // }
            calculerTotalPanier();
        }
    });
});